#!/usr/bin/env python

# Code from the Move Group Python Interface (http://docs.ros.org/en/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html) was used for this project

# Imports 
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi

# Creating a general case class that can be called to move the robot
class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        # Instantiate a RobotCommander object
        # This object is the outer-level interface to the robot
        robot = moveit_commander.RobotCommander()

        # Instantiate a PlanningSceneInterface object
        # This object is an interface to the world surrounding the robot:
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiate a MoveGroupCommander object
        # This object is an interface to one group of joints
        # Since the UR5e is being used, the group name is "manipulator"
        group_name = "manipulator"
        self.move_group = moveit_commander.MoveGroupCommander(group_name)


    # Function that takes the joint angles obtained from MoveIt as an input, and moves the robot to the position described by the joint angles
    def go_to_joint_state(self, joint_0, joint_1, joint_2, joint_3, joint_4, joint_5):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()


        joint_goal[0] = joint_0 * (pi / 180) # (pi / 180) converts the degrees input to radians
        joint_goal[1] = joint_1 * (pi / 180)
        joint_goal[2] = joint_2 * (pi / 180)
        joint_goal[3] = joint_3 * (pi / 180)
        joint_goal[4] = joint_4 * (pi / 180)
        joint_goal[5] = joint_5 * (pi / 180)

        # Move to desired joint position
        move_group.go(joint_goal, wait=True)

        # Robot stops after achieving the joint goal
        move_group.stop()


    # Function that calls a list of joint state configurations that draw the letter "A"   
    def draw_a(self):
        # Calling the MoveGroupPythonInterfaceTutorial class
        drawing_a = MoveGroupPythonInterfaceTutorial()

        drawing_a.go_to_joint_state(-22, -103, 98, -85, -89, 99)
        drawing_a.go_to_joint_state(20, -59, 45, -77, -89, 140)
        drawing_a.go_to_joint_state(59, -74, 67, -84, -89, 179)
        drawing_a.go_to_joint_state(39, -73, 67, -84, -89, 159)
        drawing_a.go_to_joint_state(-1, -91, 87, -86, -89, 119)

    # Function that calls a list of joint state configurations that draw the letter "V" 
    def draw_v(self):
        # Calling the MoveGroupPythonInterfaceTutorial class
        drawing_v = MoveGroupPythonInterfaceTutorial()

        drawing_v.go_to_joint_state(8, -10, -71, -10, -88, 102)
        drawing_v.go_to_joint_state(-13, -31, -7, -53, -88, 126)
        drawing_v.go_to_joint_state(8, -10, -71, -10, -88, 102)
        drawing_v.go_to_joint_state(15, -24, -25, -43, -88, 67)
        drawing_v.go_to_joint_state(8, -10, -71, -10, -88, 102)

    # Function that calls a list of joint state configurations that draw the letter "G" 
    def draw_g(self):
        # Calling the MoveGroupPythonInterfaceTutorial class
        drawing_g = MoveGroupPythonInterfaceTutorial()

        drawing_g.go_to_joint_state(-133, -119, -39, -105, 93, -126)
        drawing_g.go_to_joint_state(-166, -88, -85, -93, 96, -159)
        drawing_g.go_to_joint_state(-138, -48, -111, -105, 93, -131)
        drawing_g.go_to_joint_state(-109, -84, -85, -94, 90, -102)
        drawing_g.go_to_joint_state(-120, -90, -78, -94, 91, -113)
        drawing_g.go_to_joint_state(-132, -72, -96, -94, 93, -125)
 
 
 
# For running program
if __name__ == "__main__":
    # Calling class
    lets_draw = MoveGroupPythonInterfaceTutorial()

    # Drawing "A"
    lets_draw.draw_a()

    # Drawing "V"
    lets_draw.draw_v()

    # Drawing "G"
    lets_draw.draw_g()
