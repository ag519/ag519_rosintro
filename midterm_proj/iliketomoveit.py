#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi


class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        group_name = "manipulator"
        self.move_group = moveit_commander.MoveGroupCommander(group_name)

    def go_to_joint_state(self, joint_0, joint_1, joint_2, joint_3, joint_4, joint_5):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        joint_goal[0] = joint_0*(pi/180)
        joint_goal[1] = joint_1*(pi/180)
        joint_goal[2] = joint_2*(pi/180) 
        joint_goal[3] = joint_3*(pi/180)
        joint_goal[4] = joint_4*(pi/180)
        joint_goal[5] = joint_5*(pi/180) 

        move_group.go(joint_goal, wait=True)

        move_group.stop()

    def draw_a(self):
        drawing_a = MoveGroupPythonInterfaceTutorial()
        drawing_a.go_to_joint_state(0, -75, 84, -102, 0, 180)
        drawing_a.go_to_joint_state(9, -7, -69, -16, -98, 143)
        drawing_a.go_to_joint_state(15, -14, -41, -36, -98, 149)
        drawing_a.go_to_joint_state(16, -19, -29, -43, -98, 151)
        drawing_a.go_to_joint_state(-147, -134, -27, -110, 82, -134)
        drawing_a.go_to_joint_state(-147, -134, -27, -110, 82, -134)
        drawing_a.go_to_joint_state(-136, -110, -68, -96, 82, -122)
        drawing_a.go_to_joint_state(-132, -105, -75, -94, 83, -118)
        drawing_a.go_to_joint_state(-133, -107, -72, -95, 82, -119)
        drawing_a.go_to_joint_state(-133, -107, -72, -95, 82, -169)
        drawing_a.go_to_joint_state(-137, -107, -71, -95, 2, -173)
        drawing_a.go_to_joint_state(-143, -108, -69, -96, 82, -179)
        drawing_a.go_to_joint_state(-143, -108, -69, -96, 82, -179)
        drawing_a.go_to_joint_state(13, -71, 67, -87, -98, 157)


if __name__ == "__main__":
   drawing_a = MoveGroupPythonInterfaceTutorial()
   drawing_a.draw_a()



