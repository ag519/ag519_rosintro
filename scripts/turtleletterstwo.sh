#!/usr/bin/bash

rosservice call /turtle1/teleport_absolute 3 3 0

rosservice call /clear 

rosservice call /turtle1/set_pen 200 0 200 2 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	                -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	        -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	        -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	                -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	                -- '[-1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	                -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	                -- '[1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 5 3 0 ""

rosservice call /turtle2/set_pen 200 200 0 2 0

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	                        -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	                -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
			-- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	                -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	                        -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
